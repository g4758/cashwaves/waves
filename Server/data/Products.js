const products = [
  {
    name: "The WKND ",
    image: "/images/6.png",
    description:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book",
    price: 299,
    countInStock: 3,
    rating: 4,
    numReviews: 4,
  },
  {
    name: "Astro World (Women's fit)",
    image: "/images/5.png",
    description:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book",
    price: 599,
    countInStock: 10,
    rating: 4,
    numReviews: 2,
  },
  {
    name: "Astro World (Men's fit)",
    image: "/images/4.png",
    description:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book",
    price: 599,
    countInStock: 0,
    rating: 3.5,
    numReviews: 3,
  },
  {
    name: "Cash Wave Originals",
    image: "/images/3.png",
    description:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book",
    price: 899,
    countInStock: 10,
    rating: 5,
    numReviews: 9,
  },
  {
    name: "MJ x Cactus Jack",
    image: "/images/2.png",
    description:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book",
    price: 999,
    countInStock: 1,
    rating: 5,
    numReviews: 100,
  },
  {
    name: "Win like Mike Tee",
    image: "/images/1.png",
    description:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book",
    price: 999,
    countInStock: 0,
    rating: 5,
    numReviews: 105,
  },
];

export default products;
